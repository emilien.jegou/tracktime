var exec = require('cordova/exec');

window.UsageStatistics2 = {
    getUsageStatistics: function(interval, success, error) {
      	console.log("getUsageStatistics2() :: " + interval);
	  	var array = [interval];
    	exec(success, error, "MyUsageStatsManager2", "getUsageStatistics", array);
    },

	openPermissionSettings: function(success, error) {
		console.log("openPermissionSettings() :: ");
		var array = []; // not needed but seems to throw exceptions on some cases if not included.
    	exec(success, error, "MyUsageStatsManager", "openPermissionSettings", array);
	}
};
