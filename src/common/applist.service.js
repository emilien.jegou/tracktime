/*
 * applist.service.js
 * emilien <emilien@emilien-pc>, 2021
 */

import { IPubSub } from '@/common/pubsub.interface.js';
//import appAvailability from 'cordova-plugin-appavailability/www/AppAvailability.js';

class AppList extends IPubSub {
	_fetchApps() {
		return new Promise((resolve, reject) => {
			// eslint-disable-next-line
			navigator.IntentList.getList(resolve, reject);
		});
	}

	fetchAppsOnce() {
		this._fetchApps()
			.then(data => this._publish('data', data))
			.catch(err => this._publish('error', err));
	}
}

const m = new AppList();

export { m as AppList };
