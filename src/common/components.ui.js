/*
 * components.ui.js
 * emilien <emilien@emilien-pc>, 2021
 */

import { MessageBox as MB } from 'mint-ui';

export const MessageBox = (gen) => (opt) => MB({
	title: 'Erreur',
	confirmButtonText: 'Réessayer',
	cancelButtonText: 'Annuler',
	...gen, ...opt
});
