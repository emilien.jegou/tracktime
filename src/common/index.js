/*
 * index.js
 * emilien <emilien@emilien-pc>, 2021
 */

import { AppMonitor } from './monitoring.service';
import { AppList } from './applist.service';
import { Permissions } from './permissions.service';
import { User } from './user.service';
import firebase from 'firebase';
import { Plugins } from '@capacitor/core';

const { LocalNotifications } = Plugins;

import { store } from '@/store';

let currentHour = 0;
const localNotif = (hours) => {
	if (hours <= currentHour) { return; }
	currentHour = hours;
	console.log('Launching notif');
	LocalNotifications.schedule({
		notifications: [
			{
				title: `Vous avez utilisé votre téléphone ${ hours }h aujourd'hui`,
				body: 'Faites une pause de temps en temps ;)',
				id: hours,
				schedule: { at: new Date(Date.now() + 1000) },
				sound: null,
				attachments: null,
				actionTypeId: '',
				extra: null
			}
		]
	});
};

export default () => {
	const db = firebase.firestore();
	const storage = firebase.storage();

	///// APP MONITOR
	AppMonitor.subscribe('data', function({ data, same }) {
		if (!data.length) {
			store.dispatch('appUsage/updatePermission', 'ERROR');
		} else {
			if (User.user && User.user !== 'undefined') {
				const date = new Date();

				const h = Math.floor(data.filter(x => date.getDate() === x.date.getDate())
					.filter(x => date.getMonth() === x.date.getMonth())
					.filter(x => date.getYear() === x.date.getYear())
					.reduce((acc, x) => acc + x.TotalTimeInForeground, 0) / 60000 / 60);

				localNotif(h);

				db.collection('user')
					.doc(User.user.uid)
					.set({
						data: data.map(x => ({ ...x, date: x.date.getTime() }))
					}, { merge: true });
			}
		}
	});

	AppMonitor.subscribe('error', function(err) {
		console.error('AppMonitor ::', err);
	});

	// Start monitoring apps on device.
	AppMonitor.startMonitoring();

	///// PERMISSIONS
	Permissions.subscribe('data', (data) => {
		AppMonitor.ping();
		store.dispatch('appUsage/updatePermission', data);
	});

	Permissions.subscribe('error', function(err) {
		console.error('Permissions ::', err);
	});

	Permissions.checkCookies();

	///// USER

	let detach = null;
	User.subscribe('data', function(data) {
		if (data && data.uid) {
			detach && detach();
			detach = db.collection('user')
				.doc(data.uid)
				.onSnapshot((doc) => {
					const d = doc.data();
					store.dispatch('appUsage/updateAppList', d.appList);
					store.dispatch('appUsage/updateMonitoring', d.data
						.map(x => ({ ...x, date: new Date(x.date) })));
				});
		}

		store.dispatch('user/updateUser', data);
	});

	User.startUserService();

	///// APP LIST
	AppList.subscribe('data', (appList) => {
		if (User.user && User.user !== 'undefined') {
			db.collection('user')
				.doc(User.user.uid)
				.set({
					appList: appList.map(x => {
						storage.ref('icons').child(`${ x.package }`)
							.putString(x.packageIcon.slice(22, -1), 'base64', { contentType: 'image/png' });

						delete x.packageIcon;
						return x;
					})
				}, { merge: true });
		}
	});

	AppList.subscribe('error', (err) => {
		console.error('AppList ::', err);
	});

	AppList.fetchAppsOnce();

	setTimeout(() => {
		store.dispatch('setAppReady', true);
	}, 1500);
};
