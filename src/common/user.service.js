/*
 * user.service.js
 * emilien <emilien@emilien-pc>, 2021
 */

import Cookies from 'js-cookie';

import { IPubSub } from '@/common/pubsub.interface.js';
import firebase from 'firebase';

const userKey = 'user';

class User extends IPubSub {
	constructor() {
		super();
		/*
		 * Connected user
		 */
		this._user = Cookies.get(userKey);
	}

	_storeUser() {
		return (user) => {
			Cookies.set(userKey);
			this._user = user;
			this._publish('data', user);
		};
	}

	startUserService() {
		try {
			this._publish('data', this._user);

			firebase.auth().onAuthStateChanged(this._storeUser());
		} catch (e) {
			console.error(e);
		}
	}

	get user() { return this._user; }
}

const m = new User();

export { m as User };
