/*
 * monitoring.service.js
 * emilien <emilien@emilien-pc>, 2021
 */

import { IPubSub } from '@/common/pubsub.interface.js';

class AppMonitor extends IPubSub {
	constructor() {
		/*
		 * Implement publisher subscriber logic
		 */
		super();

		/*
		 * Interval between app usage data fetching.
		 */
		this._intervalDelay = 60000;

		/*
		 * ID returned by setInterval representing the timer function
		 * of the monitoring service.
		 *
		 * - null: monitoring isn't started.
		 * - != null: monitoring is currently running.
		 */
		this._intervalID = null;

		/*
		 * Action to take when an error is catched by the monitoring service,
		 * can be overwriten from startMonitoring default args.
		 */
		this._errorCallback = console.error;

		/*
		 * Monitoring data.
		 */
		this._data = null;
	}

	_getMonitoringData() {
		return new Promise((resolve, reject) => {
			// eslint-disable-next-line
			UsageStatistics.getUsageStatistics('Daily', resolve, reject);
		});
	}

	_filterData(data) {
		// Only keep apps used more than two minutes
		return data.filter(x => (x.TotalTimeInForeground >= 120000))
			.map(x => ({ ...x, date: new Date(x.LastTimeUsed) }))
			.sort((a, b) => b.date - a.date);
	}

	_storeMonitoringData(data) {
		const d = this._filterData(JSON.parse(data));

		const same = (JSON.stringify(this._data) === JSON.stringify(d));
		this._data = d;
		this._publish('data', { data: this._data, same });
	}

	_monitoringFunction() {
		return () => {
			this._getMonitoringData()
				.then((data) => this._storeMonitoringData(data))
				.catch((err) => {
					this.stopMonitoring();
					this._publish('error', err);
					this._errorCallback(err);
				});
		};
	}

	startMonitoring(errorCallback) {
		if (errorCallback) { this._errorCallback = errorCallback; }
		this._monitoringFunction()();
		this._intervalID = window.setInterval(
			this._monitoringFunction(),
			this._intervalDelay
		);
		return this;
	}

	ping() {
		this._monitoringFunction()();
	}

	stopMonitoring() {
		return new Promise((resolve, reject) => {
			if (this.intervalFunction === null) {
				return reject('Tried to stop unactive monitoring service');
			}

			window.clearInterval(this._intervalID);
			this.intervalID = null;
			resolve();
		});
	}
}

const m = new AppMonitor();

export { m as AppMonitor };
