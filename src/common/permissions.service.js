/*
 * permission.service.js
 * emilien <emilien@emilien-pc>, 2021
 */

import Cookies from 'js-cookie';

import { IPubSub } from '@/common/pubsub.interface.js';

const permissionKey = 'permissions';

class Permissions extends IPubSub {
	checkCookies() {
		this._publish('data', Cookies.get(permissionKey));
	}

	askMonitoringPermission() {
		return new Promise((resolve, reject) => {
			// eslint-disable-next-line
			UsageStatistics
				.openPermissionSettings(
					(data) => {
						Cookies.set(permissionKey, data);
						this._publish('data', data);
						resolve(data);
					},
					(err) => {
						this._publish('error');
						reject(err);
					}
				);
		});
	}
}

const m = new Permissions();

export { m as Permissions };
