/*
 * pubsub.interface.js
 * emilien <emilien@emilien-pc>, 2021
 */

export class IPubSub {
	constructor() {
		/*
		 * Events subscribers callbacks, ex:
		 * {
		 *    'eventName': [ subcb1, subcb2, ... ],
		 *    ...
		 * }
		 */
		this._callbacks = {};

		/*
		 * Events Id's, increment for each new events
		 */
		this._idIdx = 0;
	}

	subscribe(ev, callback) {
		if (this._callbacks[ev] === undefined) {
			this._callbacks[ev] = {};
		}
		const id = this.Idx++;

		this._callbacks[ev][id] = callback;
		return id;
	}

	/* You still need to provide event type for unsubscribe */
	unsubscribe(ev, id) {
		if (!this._callbacks[ev][id]) {
			throw Error(`PubSub: Tried to delete a null event for eventName: ${ ev }`);
		}
		delete this._callbacks[ev][id];
	}

	_publish(ev, data) {
		const l = this._callbacks[ev];

		if (l === null) { return; }

		for (const it of Object.values(l)) {
			it.apply(this, [data]);
		}
	}
}

