/*
 * index.js
 * emilien <emilien@emilien-pc>, 2021
 */

import Vue from 'vue';
import Vuex from 'vuex';

import appUsage from './modules/app-usage';
import user from './modules/user';
import settings from './modules/settings';

Vue.use(Vuex);

export const store = new Vuex.Store({
	modules: {
		appUsage,
		settings,
		user
	},
	state: {
		/* App is not yet ready to be used */
		app_ready: false,
	},
	mutations: {
		APP_READY: (state, data) => {
			state.app_ready = data;
		},
	},
	actions: {
		setAppReady({ commit }, data) {
			commit('APP_READY', data);
		},
	}
});
