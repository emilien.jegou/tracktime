/*
 * app-usage.js
 * emilien <emilien@emilien-pc>, 2021
 */

import Cookies from 'js-cookie';

const state = {
	/*
	 * Period in which to show monitoring
	 * should be undefined if not set and not null
	 */
	period: Cookies.get('period')
};

const mutations = {
	UPDATE_PERIOD: (state, data) => {
		Cookies.set('period', data);
		console.log('SET PERIOD!!!', data);
		state.period = data;
	},
};

const actions = {
	updatePeriod({ commit }, data) {
		commit('UPDATE_PERIOD', data);
	},
};

export default {
	namespaced: true,
	state,
	mutations,
	actions
};
