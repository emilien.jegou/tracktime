/*
 * app-usage.js
 * emilien <emilien@emilien-pc>, 2021
 */

const state = {
	/*
	 * User info fetched from firebase.
	 */
	data: null,

	/*
	 * Check if user has set permission or not.
	 */
	permission: null,
};

const mutations = {
	UPDATE_DATA: (state, data) => {
		state.data = data;
	},
};

const actions = {
	updateUser({ commit }, data) {
		commit('UPDATE_DATA', data);
	},
	updatePermission({ commit }, data) {
		commit('UPDATE_PERMISSION', data);
	},
};

export default {
	namespaced: true,
	state,
	//getters,
	mutations,
	actions
};

