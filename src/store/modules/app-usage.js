/*
 * app-usage.js
 * emilien <emilien@emilien-pc>, 2021
 */

const state = {
	/*
	 * Monitoring data
	 */
	monitoring: [],

	/*
	 * Whether user has permissions,
	 * Currently false if data is an empty array. Could make use of the
	 * capacitor permission plugins instead.
	 */
	permission: false,

	/*
	 * List of all apps installed on device with icons
	 */
	appList: [],
};

const getters = {
	dates: ({ monitoring }) => (
		Object.values(monitoring
			.map(({ date }) => new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0))
			.reduce((acc, x) => ({ ...acc, [x]: x }), {}))
	),
	monitoringDate: ({ monitoring }) => (date) => (
		monitoring.filter(x => date.getDate() === x.date.getDate())
			.filter(x => date.getMonth() === x.date.getMonth())
			.filter(x => date.getYear() === x.date.getYear())
	),
	app: ({ appList }) => (id) => (
		appList.find(x => x.package === id)
	),

	/* Get total amount of phone usage for one day */
	dayTot: ({ monitoring }) => (date) => (
		monitoring.filter(x => date.getDate() === x.date.getDate())
			.filter(x => date.getMonth() === x.date.getMonth())
			.filter(x => date.getYear() === x.date.getYear())
			.reduce((acc, x) => acc + x.TotalTimeInForeground, 0)
	)
};

const mutations = {
	UPDATE_MONITORING: (state, data) => {
		state.monitoring = data;
	},
	UPDATE_PERMISSION: (state, data) => {
		state.permission = data;
	},
	UPDATE_APP_LIST: (state, data) => {
		state.appList = data;
	}
};

const actions = {
	updateMonitoring({ commit }, data) {
		commit('UPDATE_MONITORING', data);
	},
	updatePermission({ commit }, data) {
		commit('UPDATE_PERMISSION', data);
	},
	updateAppList({ commit }, data) {
		commit('UPDATE_APP_LIST', data);
	},
};

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
};
