/*
 * time.js
 * emilien <emilien@emilien-pc>, 2021
 */

export const pzero = (s, n = 2, c = '0') => String(s).padStart(n, c);

export const format_hhmm = (h, m) => `${ pzero(h) }:${ pzero(m) }`;

// eslint-disable-next-line
const months_en = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const months_fr = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];

export const format_date = (date) => `${ date.getDate() } ${ months_fr[date.getMonth()] } ${ date.getFullYear() }`;

export const format_time = (msecs) => {
	const minutes = Math.floor(msecs / 60000);

	if (minutes > 60) {
		return `${ Math.floor(minutes / 60) }h ${ minutes % 60 }m`;
	}
	return `${ minutes } mins`;
};
