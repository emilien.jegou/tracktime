<template>
	<div>
		<img id="time_logo" src="@/assets/time_logo.png" alt="limit_logo" />
		<LayoutHeader
			title="Surveillance"
			desc="Surveiller votre temps d'écran selon l'heure de la journée"
		/>
		<div class="flex gap-1 column mt-3 pb-2">
			<div class="flex column gap-1">
				<h4> Période d'activation</h4>
				<div>
					<mt-picker id="picker" :slots="slots" @change="pickerSelect" />
				</div>
			</div>
		</div>
		<ButtonArea>
			<a class="w-100 md" @click="setSettings">
				<button>Activer la surveillance</button>
			</a>
			<a class="w-100 mt-1 md center-text" @click="setSettingsNull">
				Ignorer
			</a>
		</ButtonArea>
	</div>
</template>

<script>
import ButtonArea from '@/components/ButtonArea';
import LayoutHeader from '@/components/LayoutHeader';

export default {
	name: 'Parental',
	components: { ButtonArea, LayoutHeader },
	data() {
		const values = (() => {
			const zeroPad = (num, places) => String(num).padStart(places, '0');

			return Array.from({ length: 48 }, (_, i) => i * 30)
				.map(x => ([ Math.floor(x / 60), x % 60 ]))
				.map(([ hours, minutes ]) =>
					`${ zeroPad(hours, 2) }:${ zeroPad(minutes, 2) }`);
		})();

		const defaultValue = [ 18, 34 ];
		return {
			value: defaultValue,
			slots: [
				{
					flex: 1,
					values: values.slice(0, -1),
					className: 'slot1',
					defaultIndex: defaultValue[0],
					textAlign: 'right'
				}, {
					divider: true,
					content: '-',
					className: 'slot2'
				}, {
					flex: 1,
					values,
					className: 'slot3',
					defaultIndex: defaultValue[1],
					textAlign: 'left'
				}
			]
		};
	},
	methods: {
		pickerSelect(picker, values) {
			if (values[0] > values[1]) {
				picker.setSlotValue(1, values[0] + 1);
				this.value = [ values[0], values[0] + 1 ];
			} else {
				this.value = [ values[0], values[1] ];
			}
		},
		setSettings() {
			this.$store.dispatch('settings/updatePeriod', this.value);
			this.$router.push('/usage');
		},
		setSettingsNull() {
			this.$store.dispatch('settings/updatePeriod', null);
			this.$router.push('/usage');
		}
	}
};
</script>

<style lang="scss" scoped>

#time_logo{
  width: 90px;
  height: auto;
  margin: auto;
}

#picker{
  padding-top: 20px;
}

</style>
