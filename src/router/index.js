/*
 * index.js
 * emilien <emilien@emilien-pc>, 2021
 */

import Vue from 'vue';
import Router from 'vue-router';
import Layout from '@/components/Layout';
import NProgress from 'nprogress';
import { setPageTitle } from '@/utils/page-title';

Vue.use(Router);

const middlewares = [
	require('./middlewares/permissions'),
];

const inlineMiddleware = (module) => {
	return (to, from, next) => module.default(to, from, next);
};

const router = new Router({
	routes: [
		{
			path: '/',
			component: Layout,
			hidden: true,
			children: [
				{
					path: '/',
					name: 'Greeter',
					component: () => import('@/views/Greeter')
				},
				{
					path: '/login',
					name: 'Login',
					component: () => import('@/views/Login')
				},
				{
					path: '/signup',
					name: 'Signup',
					component: () => import('@/views/Signup')
				},
				{
					path: '/retrieve',
					name: 'RetrievePassword',
					component: () => import('@/views/RetrievePassword')
				},
				{
					path: '/parental',
					name: 'Parental',
					component: () => import('@/views/Parental'),
					beforeEnter: inlineMiddleware(require('./middlewares/settings')),
				},
				{
					path: '/usage',
					name: 'AllowUsageInspect',
					component: () => import('@/views/AllowUsageInspect'),
					beforeEnter: inlineMiddleware(require('./middlewares/usage-inspect')),
				},
				{
					path: '/limit',
					name: 'LimitScreen',
					component: () => import('@/views/LimitScreen')
				},
				{
					path: '/parameter',
					name: 'Parameters',
					component: () => import('@/views/Parameters')
				},
				//{
				//path: '/password',
				//name: 'Password',
				//component: () => import('@/views/Password')
				//}
			]
		},
		{
			path: '/homepage',
			name: 'Homepage',
			component: () => import('@/views/Homepage')
		}
	]
});

router.beforeEach(async(to, from, next) => {
	NProgress.start();
	setPageTitle(to.meta.title);

	for (const middleware of middlewares.map(x => x.default)) {
		middleware(to, from, next);
	}

	next();
});

router.afterEach(() => {
	NProgress.done();
});

export { router };
