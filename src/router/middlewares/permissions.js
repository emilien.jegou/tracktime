/*
 * permissions.js
 * emilien <emilien@emilien-pc>, 2021
 */

//import firebase from 'firebase';
import { store } from '@/store/';

const redirectList = [
	'/',
	'/login',
	'/signup',
	'/retrieve',
];

const whiteList = [
	...redirectList,
];

export default function(to, from, next) {
	const user = store.state.user.data;

	if (user) {
		if (redirectList.indexOf(to.path) !== -1) {
			next({ path: '/parental' });
		} else {
			next();
		}
	} else {
		if (whiteList.indexOf(to.path) !== -1) {
			console.log('Maybe There???');
			next();
		} else {
			// other pages that do not have permission to access are redirected to the login page.
			next(`/login?redirect=${ to.path }`);
		}
	}
}
