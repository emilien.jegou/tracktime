/*
 * usage-inspect.js
 * emilien <emilien@emilien-pc>, 2021
 */

import { store } from '@/store/';

export default (_to, _from, next) => {
	if (store.state.appUsage.permission === 'OK') {
		next({ path: '/homepage' });
	} else {
		next();
	}
};
