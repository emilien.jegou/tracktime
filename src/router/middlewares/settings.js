/*
 * settings.js
 * emilien <emilien@emilien-pc>, 2021
 */

import { store } from '@/store/';

export default (_to, _from, next) => {
	console.log(store.state.settings.period);
	if (store.state.settings.period !== undefined) {
		next({ path: '/usage' });
	} else {
		next();
	}
};
