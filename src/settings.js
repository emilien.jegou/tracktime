/*
 * settings.js
 * emilien <emilien@emilien-pc>, 2021
 */

/**
 * @type {string}
 * @description Shown page title
 */
module.exports.title = 'Tracktime';
