/*
 * main.js
 * emilien <emilien@emilien-pc>, 2021
 */

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueMeta from 'vue-meta';
import firebase from 'firebase/app';

const firebaseConfig = {
	apiKey: 'AIzaSyCOC3ilj93Yf11j22J6JcPHoFZXEnWZycE',
	authDomain: 'timetrack-30d54.firebaseapp.com',
	projectId: 'timetrack-30d54',
	storageBucket: 'timetrack-30d54.appspot.com',
	messagingSenderId: '75724287936',
	appId: '1:75724287936:web:68621445c4c6e8908ee225'
};

firebase.initializeApp(firebaseConfig);

import 'firebase/auth';
import 'firebase/firestore';

import 'normalize.css/normalize.css';
import { Picker, Popup, InfiniteScroll } from 'mint-ui';

import NProgress from 'nprogress';

NProgress.configure({ showSpinner: false });

/*
 * mint-ui styles
 * Only import use components.
 */
import 'mint-ui/lib/picker/style.css';
import 'mint-ui/lib/message-box/style.css';
import 'mint-ui/lib/indicator/style.css';
import 'mint-ui/lib/infinite-scroll/style.css';
import 'mint-ui/lib/popup/style.css';

import 'nprogress/nprogress.css';

import '@/styles/global.scss';
import '@/styles/mint-ui-dark-fix.scss';
import tasks from '@/common/';

Vue.component(Picker.name, Picker);
Vue.component(Popup.name, Popup);
Vue.use(InfiniteScroll);

console.log('Starting app');
import App from './App';

import { store } from './store';
import { router } from './router';

Vue.use(VueMeta, {
	metaInfo: {
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' }
		]
	}
});

Vue.use(VueRouter);
Vue.config.productionTip = false;

// vh hotfix for mobile
const vhHotfix = () =>
	document.documentElement.style
		.setProperty('--vh', `${ window.innerHeight * 0.01 }px`);

window.addEventListener('resize', vhHotfix);
vhHotfix();

Vue.config.productionTip = false;

tasks();

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');
