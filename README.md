# tracktime

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Cordova Research

- https://github.com/KIvanow/installedApps
- https://github.com/jmrezayi2/Applist
- https://www.npmjs.com/package/cordova-plugin-applist
- https://github.com/ohh2ahh/AppAvailability

- It returns applications list with Base64 encoded icons.: https://github.com/nick-denry/cordova-plugin-intent-list
- https://github.com/clelland/cordova-plugin-external-app-block
- https://github.com/katzer/cordova-plugin-local-notifications
