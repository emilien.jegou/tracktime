/*
 * postcss.config.js
 * emilien <emilien@emilien-pc>, 2021
 */

// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
	'plugins': {
		// to edit target browsers: use "browserslist" field in package.json
		'autoprefixer': {}
	}
}
